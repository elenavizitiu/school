app.controller('newTeacherCtrl', function($scope, data, $uibModalInstance, $http) {
    $scope.form = {};
    if(data){
        $scope.currentTeacher = data;
        $scope.currentTeacher.birthdate = new Date(moment($scope.currentTeacher.birthdate).format('MM/DD/YYYY'));

    }
    else{
        $scope.currentTeacher = {
            first_name: '',
            last_name: '',
            email: '',
            birthdate: ''
        };
    }
    $scope.submitForm = function () {
        var object = angular.copy($scope.currentTeacher);
        object.birthdate = new Date(moment($scope.currentTeacher.birthdate).format('MM/DD/YYYY'));
        if(data){
            $scope.currentTeacher.action = 'edit';
            action(object);
        }
        else{
            action(object);
            //new
        }
        $uibModalInstance.close($scope.currentTeacher);
        $scope.currentTeacher.birthdate = moment($scope.currentTeacher.birthdate).format('MM/DD/YYYY');

    };

    var action = function(data){
        var method = 'POST';
        var url = 'http://myhost/school/backend/teacher.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: data,
        }).then(function (success) {
        }, function (error) {

        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});