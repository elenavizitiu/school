app.controller('classCtrl', function($scope, $uibModal, $http) {
    var getClass = function(){

        var method = 'GET';
        var url = 'http://myhost/school/backend/classAndGrade.php?getTable=class';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
        }).then(function (success) {
            $scope.classList = success.data;
        }, function (error) {

        });
    };

    getClass();

    $scope.addNewClass = function(){
        var modalInstance = $uibModal.open({
            templateUrl: '../page/newClass.html',
            controller: 'newClassCtrl',
            scope: $scope,
            resolve: {
                data: function () {
                    return '';
                }
            }
        });
        modalInstance.result.then(function (success) {
            $scope.classList.splice($scope.classList, 0, success);
        }, function (error) {
        });
    };

    $scope.status = {
        open: false
    };
});