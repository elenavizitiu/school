app.controller('menuCtrl', function ($scope, $location) {
    $scope.logout = function () {
        localStorage.clear();
        $location.path('/login');
      //logout: clear localstorage
      //redirect catre login
    };
    $scope.afiseaza = function() {
        $scope.data_storage = localStorage.getItem('data_login');
        if ($scope.data_storage == null) {
            $scope.afiseaza_login = true;
            $scope.afiseaza_logout = false;
        } else {
            $scope.afiseaza_login = false;
            $scope.afiseaza_logout = true;
        }
    };

    $scope.$on('$routeChangeStart', function() {
        $scope.afiseaza();
    });

});