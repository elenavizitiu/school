app.controller('newStudentCtrl', function($scope, data, $uibModalInstance, $http) {
    $scope.form = {};

    if(data){
        $scope.currentStudent = data;
        $scope.currentStudent.birthdate = new Date(moment($scope.currentStudent.birthdate).format('MM/DD/YYYY'));
    }
    else{
        $scope.currentStudent = {
            first_name: '',
            last_name: '',
            email: '',
            birthdate: '',
            class_name: ''
        };
    }
    $scope.submitForm = function () {
        var object = angular.copy($scope.currentStudent);
        object.birthdate = new Date(moment($scope.currentStudent.birthdate).format('MM/DD/YYYY'));
        if(data){
            $scope.currentStudent.action = 'edit';
            action(object);
        }
        else{
            action(object);
            //new
        }
        $uibModalInstance.close($scope.currentStudent);
        $scope.currentStudent.birthdate = moment($scope.currentStudent.birthdate).format('MM/DD/YYYY');
    };

    var action = function(data){
        var method = 'POST';
        var url = 'http://myhost/school/backend/student.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: data,
        }).then(function (success) {
            debugger;
        }, function (error) {

        });
    };

    var getClasses = function(){
        var method = 'GET';
        var url = 'http://myhost/school/backend/classAndGrade.php?getTable=class';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(function (success) {
            $scope.listOfClasses = success.data;
        }, function (error) {

        });
    };
    getClasses();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});