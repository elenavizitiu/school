app.controller('gradesCtrl', function($scope, $uibModal, $http) {

    $scope.status = {
        open: false
    };

    var getStudents = function(){
        var method = 'GET';
        var url = 'http://myhost/school/backend/student.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'

            },
        }).then(function (success) {
            $scope.studentsList = success.data;
        }, function (error) {

        });
    };

    getStudents();

    var getStudentGrades = function(){
        var method = 'GET';
        var url = 'http://myhost/school/backend/studentGrade.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
        }).then(function (success) {
            $scope.studentGrades = success.data;
            angular.forEach($scope.studentsList, function(student){
                angular.forEach($scope.studentGrades, function(stud){
                    if(student.id == stud.studentId){
                        stud.name = student.first_name + ' ' + student.last_name;
                    }
                })
            });
        }, function (error) {

        });
    };
    getStudentGrades();




    $scope.addNewGrade = function(){
        var modalInstance = $uibModal.open({
            templateUrl: '../page/newGrade.html',
            controller: 'newGradeCtrl',
            scope: $scope,
            resolve: {
                data: function () {
                    return '';
                }
            },
        });
        modalInstance.result.then(function (success) {
            $scope.gradesList.splice($scope.gradesList, 0, success);
        }, function () {
        });
    };



});