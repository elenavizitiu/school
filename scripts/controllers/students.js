app.controller('studentsCtrl', function($scope, $uibModal, $http) {
    var getStudents = function(){
        var method = 'GET';
        var url = 'http://myhost/school/backend/student.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'

            },
        }).then(function (success) {
            $scope.studentsList = success.data;
        }, function (error) {

        });
    };

    getStudents();

    $scope.status = {
        open: false
    };
    $scope.addNewStudent = function(){
        var modalInstance = $uibModal.open({
            templateUrl: '../page/newStudent.html',
            controller: 'newStudentCtrl',
            scope: $scope,
            resolve: {
                data: function () {
                    return '';
                }
            }
        });
        modalInstance.result.then(function (success) {
            $scope.studentsList.splice($scope.studentsList, 0, success);
        }, function (error) {
      });
    };

    $scope.editStudent = function (student) {
        var modalInstance = $uibModal.open({
            templateUrl: '../page/newStudent.html',
            controller: 'newStudentCtrl',
            scope: $scope,
            resolve: {
                data: function () {
                    return student;
                }
            }
        });
        modalInstance.result.then(function (success) {
            debugger;
            $scope.studentsList.splice($scope.studentsList.indexOf(student), 1, success);
        }, function (error) {
        });
    };

    $scope.removeStudent = function (student) {
        student.action = 'remove';
        var method = 'POST';
        var url = 'http://myhost/school/backend/student.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: student,
        }).then(function (success) {
            $scope.studentsList.splice($scope.studentsList.indexOf(student), 1);
        }, function (error) {

        });
        //remove student;
    }
});