app.controller('teachersCtrl', function($scope, $uibModal, $http) {

    $scope.status = {
        open: false
    };

    var getTeachers = function(){
        var method = 'GET';
        var url = 'http://myhost/school/backend/teacher.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'

            },
        }).then(function (success) {
            $scope.teachersList = success.data;
        }, function (error) {

        });
    };

    getTeachers();

    $scope.addNewTeacher = function(){
        var modalInstance = $uibModal.open({
            templateUrl: '../page/newTeacher.html',
            controller: 'newTeacherCtrl',
            resolve: {
                data: function () {
                    return '';
                }
            }
        });
        modalInstance.result.then(function (success) {
            $scope.teachersList.splice($scope.teachersList, 0, success);
        }, function (error) {
        });
    };

    $scope.editTeacher = function (teacher) {
        var modalInstance = $uibModal.open({
            templateUrl: '../page/newTeacher.html',
            controller: 'newTeacherCtrl',
            scope: $scope,
            resolve: {
                data: function () {
                    return teacher;
                }
            }
        });
        modalInstance.result.then(function (success) {
            $scope.teachersList.splice($scope.teachersList.indexOf(teacher), 1, success);
        }, function (error) {
        });
    };

    $scope.removeTeacher = function (teacher) {
        teacher.action = 'remove';
        var method = 'POST';
        var url = 'http://myhost/school/backend/teacher.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: teacher,
        }).then(function (success) {
            $scope.teachersList.splice($scope.teachersList.indexOf(teacher), 1);
        }, function (error) {

        });
        //remove student;
    }

});