app.controller('newClassCtrl', function($scope, data, $uibModalInstance, $http) {
    $scope.form = {};

        $scope.currentClass = {
            class: ''
        };
    $scope.submitForm = function () {
        action($scope.currentClass);
        $uibModalInstance.close($scope.currentClass);
    };

    var action = function(data){
        var method = 'POST';
        var url = 'http://myhost/school/backend/classAndGrade.php?getTable=class';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: data,
        }).then(function (success) {
        }, function (error) {

        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});