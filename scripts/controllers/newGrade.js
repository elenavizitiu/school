app.controller('newGradeCtrl', function($scope, $uibModalInstance, $http) {
    $scope.form = {};
    $scope.newGrade ={
        grade: '',
        studentId: ''
    };
    $scope.submitForm = function () {
        if ($scope.form.userForm.$valid) {
            var object = {
                grades: [],
                studentId: $scope.newGrade.studentId
            };
            object.grades.push($scope.newGrade.grade)
            action(object);
            // add grade in db
            $uibModalInstance.close($scope.newGrade);
        } else {
        }
    };

    var getStudents = function(){
        var method = 'GET';
        var url = 'http://myhost/school/backend/student.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'

            },
        }).then(function (success) {
            $scope.studentsList = success.data;
        }, function (error) {

        });
    };

    getStudents();


    var getGrades = function(){

        var method = 'GET';
        var url = 'http://myhost/school/backend/classAndGrade.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
        }).then(function (success) {
            $scope.gradesList = success.data;
        }, function (error) {

        });
    };

    getGrades();

    var action = function(data){
        var method = 'POST';
        var url = 'http://myhost/school/backend/studentGrade.php';
        $http({
            method: method,
            url: url,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: data,
        }).then(function (success) {
        }, function (error) {

        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});