var app = angular.module('app', ['ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap', 'angularjsToast','ngTouch']);
var Const = {
    api_base : 'https://api-test.insoftd.com/v1/',
    api_client : 'https://api-test.insoftd.com/v1/client/'
};
app.config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl : '../page/students.html',
        controller : 'studentsCtrl'
        })
        .when('/student',{
                templateUrl : '../page/students.html',
                controller : 'studentsCtrl'
        })
        .when('/teacher',{
                templateUrl : '../page/teachers.html',
                controller : 'teachersCtrl'
            })
        .when('/grade',{
            templateUrl : '../page/grades.html',
            controller : 'gradesCtrl'
        })
        .when('/class',{
            templateUrl : '../page/class.html',
            controller : 'classCtrl'
        });
    $locationProvider.hashPrefix("");
});
app.run(function ($rootScope, $location) {

});

