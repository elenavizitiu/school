<?php

require_once "connection.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
 $json_params = file_get_contents("php://input");
    $decoded_params = json_decode($json_params);
	if(isset($decoded_params->class)) {
		$sql="INSERT INTO class (name) VALUES ('$decoded_params->class')";
	} else {
		$sql="INSERT INTO grades (grade) VALUES ('$decoded_params->grade')";
	}

	
	if ($conn->query($sql) === TRUE) {
		echo json_encode(array('success' => TRUE));
	} else {
		echo json_encode(array('success' => FALSE));
	}

} else {
	$result = getGrades($conn);
	if(isset($_GET['getTable']) && $_GET['getTable'] == "class") {
		$result = getClasses($conn);
	}

	echo json_encode($result);
}

$conn->close();

//GRADES
function getGrades($conn) {
	$sql = "SELECT * FROM grades";
	$result = $conn->query($sql);

	$grades = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($grades, array('grade' => $row["grade"]));
	  }
	}
	return $grades;
}

//CLASSES
function getClasses($conn) {
	$sql = "SELECT * FROM class";
	$result = $conn->query($sql);

	$classes = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($classes, array('class' => $row["name"]));
	  }
	}
	return $classes;
}

?>
