<?php

require_once "connection.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $json_params = file_get_contents("php://input");
    $decoded_params = json_decode($json_params);
	$grades = $decoded_params->grades;
	$studentId = $decoded_params->studentId;
	$sql="";

	foreach($grades as $grade) {
      $sql.="INSERT INTO studentgraderelation (student_id, grade) VALUES ('$studentId', '$grade'); ";
    }

	if (mysqli_multi_query($conn, $sql)) {
		echo json_encode(array('success' => TRUE));
	} else {
		echo json_encode(array('success' => FALSE));
	}

} else {
    $result = getAll($conn);

	if(isset($_GET['studentId'])) {
		$result = getAllGrades($conn);
	}

	if(isset($_GET['grade'])) {
		$result = getAllStudents($conn);
	}

	if(empty($result)) {
		echo json_encode(array('success' => FALSE));
	} else {
		echo json_encode($result);
	}
}

$conn->close();


function getAllGrades($conn) {
	$sql = "SELECT * FROM studentgraderelation WHERE student_id = '$_GET[studentId]' ";
	$result = $conn->query($sql);

	$grades = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($grades, array('grade' => $row["grade"]));
	  }
	}
	return $grades;
}

function getAllStudents($conn) {
	$sql = "SELECT * FROM studentgraderelation WHERE grade = '$_GET[grade]' ";
	$result = $conn->query($sql);

	$students = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($students, array('studentId' => $row["student_id"]));
	  }
	}
	return $students;
}

function getAll($conn) {
	$sql = "SELECT * FROM studentgraderelation";
	$result = $conn->query($sql);

	$students = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($students, array('studentId' => $row["student_id"], 'grade' => $row["grade"]));
	  }
	}
	return $students;
}

?>
