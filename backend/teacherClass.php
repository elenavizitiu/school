<?php

require_once "connection.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {

	$classes = json_decode($_POST['classes']);
	$teacherId = $_POST['teacherId'];
	$sql="";

	foreach($classes as $class) {
      $sql.="INSERT INTO teacherclassrelation (teacher_id, class) VALUES ('$teacherId', '$class'); ";
    }

	if (mysqli_multi_query($conn, $sql)) {
		echo json_encode(array('success' => TRUE));
	} else {
		echo json_encode(array('success' => FALSE));
	}

} else {
	$result = "";
	if(isset($_GET['teacherId'])) {
		$result = getAllClasses($conn);
	}

	if(isset($_GET['class'])) {
		$result = getAllTeachers($conn);
	}

	if(empty($result)) {
		echo json_encode(array('success' => FALSE));
	} else {
		echo json_encode($result);
	}
}

$conn->close();


function getAllClasses($conn) {
	$sql = "SELECT * FROM teacherclassrelation WHERE teacher_id = '$_GET[teacherId]' ";
	$result = $conn->query($sql);

	$classes = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($classes, array('class' => $row["class"]));
	  }
	}
	return $classes;
}

function getAllTeachers($conn) {
	$sql = "SELECT * FROM teacherclassrelation WHERE class = '$_GET[class]' ";
	$result = $conn->query($sql);

	$teachers = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
	    	array_push($teachers, array('teacherId' => $row["teacher_id"]));
	  }
	}
	return $teachers;
}

?>
