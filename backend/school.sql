-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: oct. 18, 2020 la 01:59 PM
-- Versiune server: 10.4.14-MariaDB
-- Versiune PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `school`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `class`
--

CREATE TABLE `class` (
  `name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `class`
--

INSERT INTO `class` (`name`) VALUES
('10A'),
('10B'),
('11A'),
('11B'),
('12A'),
('12B'),
('9A'),
('9B');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `grades`
--

CREATE TABLE `grades` (
  `grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `grades`
--

INSERT INTO `grades` (`grade`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(60) NOT NULL,
  `class_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `student`
--

INSERT INTO `student` (`id`, `last_name`, `first_name`, `birthdate`, `email`, `class_name`) VALUES
(1, 'Popescu', 'Maria', '1990-10-13', 'test@yahoo.com', '10A'),
(2, 'Ionescu', 'Elena', '1994-11-11', 'ionescue@yahoo.com', '10B'),
(9, 'Eminescu', 'Mihai', '1910-01-14', 'eminescu@test.com', '11B'),
(10, 'Eminescu', 'Mihai', '1910-01-14', 'eminescu@test.com', '11B');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `studentgraderelation`
--

CREATE TABLE `studentgraderelation` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `studentgraderelation`
--

INSERT INTO `studentgraderelation` (`id`, `student_id`, `grade`) VALUES
(1, 1, 7),
(2, 1, 8),
(3, 1, 4),
(4, 2, 2),
(5, 2, 9),
(8, 2, 10),
(9, 2, 7);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `teacher`
--

INSERT INTO `teacher` (`id`, `first_name`, `last_name`, `birthdate`, `email`) VALUES
(1, 'Sadoveanu', 'Mihail', '1930-10-10', 'sadoveanu@gmail.com'),
(3, '', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `teacherclassrelation`
--

CREATE TABLE `teacherclassrelation` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `class` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `teacherclassrelation`
--

INSERT INTO `teacherclassrelation` (`id`, `teacher_id`, `class`) VALUES
(1, 1, '11A'),
(2, 1, '10A'),
(3, 1, '9B'),
(4, 1, '11B');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`name`);

--
-- Indexuri pentru tabele `grades`
--
ALTER TABLE `grades`
  ADD UNIQUE KEY `grade` (`grade`),
  ADD UNIQUE KEY `grade_2` (`grade`),
  ADD UNIQUE KEY `grade_3` (`grade`);

--
-- Indexuri pentru tabele `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class` (`class_name`);

--
-- Indexuri pentru tabele `studentgraderelation`
--
ALTER TABLE `studentgraderelation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade` (`grade`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexuri pentru tabele `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexuri pentru tabele `teacherclassrelation`
--
ALTER TABLE `teacherclassrelation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `class_id` (`class`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pentru tabele `studentgraderelation`
--
ALTER TABLE `studentgraderelation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pentru tabele `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pentru tabele `teacherclassrelation`
--
ALTER TABLE `teacherclassrelation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constrângeri pentru tabele eliminate
--

--
-- Constrângeri pentru tabele `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `class` FOREIGN KEY (`class_name`) REFERENCES `class` (`name`);

--
-- Constrângeri pentru tabele `studentgraderelation`
--
ALTER TABLE `studentgraderelation`
  ADD CONSTRAINT `grade` FOREIGN KEY (`grade`) REFERENCES `grades` (`grade`),
  ADD CONSTRAINT `student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`);

--
-- Constrângeri pentru tabele `teacherclassrelation`
--
ALTER TABLE `teacherclassrelation`
  ADD CONSTRAINT `class_id` FOREIGN KEY (`class`) REFERENCES `class` (`name`),
  ADD CONSTRAINT `teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
