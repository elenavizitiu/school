<?php

require_once "connection.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
 $json_params = file_get_contents("php://input");
    $decoded_params = json_decode($json_params);
	if( isset($decoded_params->action)) {
    		if($decoded_params->action == "edit") {
		$sql = "UPDATE teacher SET last_name='$decoded_params->last_name',
				first_name='$decoded_params->first_name', birthdate='$decoded_params->birthdate',
				email='$decoded_params->email'
			WHERE id='$decoded_params->id'";
	        }
            	if($decoded_params->action == "remove") {
                $sql="DELETE FROM teacher WHERE id='$decoded_params->id' ";
            }
           }
             else {
                $sql="INSERT INTO teacher (last_name, first_name, birthdate, email)
                VALUES
                ('$decoded_params->last_name', '$decoded_params->first_name', '$decoded_params->birthdate', '$decoded_params->email')";
            }

	if ($conn->query($sql) === TRUE) {
		echo json_encode(array('success' => $decoded_params));
	} else {
		echo json_encode(array('success' => $decoded_params));
	}
} else {
	$sql = "SELECT * FROM teacher";
	$result = $conn->query($sql);

	$teachers = [];
	if ($result->num_rows > 0) {
	  // output data of each row
		
		while($row = $result->fetch_assoc()) {
	    	array_push($teachers, array('id' => $row["id"],
	    						'first_name' => $row["first_name"],
	    						'last_name' => $row["last_name"],
	    						'birthdate' => $row["birthdate"],
	    						'email' => $row["email"] ));
	  }
	}
	echo json_encode($teachers);
}

$conn->close();

?>
